### 前台主要功能介绍

* 首页功能：导航模块，轮播模块，专区模块，友情链接，底部信息
* 课程中心：搜索功能，分类导航（自定义分类设置），课程列出
* 课程详情：课程信息，课程目录，讲师信息、购买、播放等功能
* 个人中心：我的课程，我的订单，个人信息

### 后台主要功能介绍

* 权限管理功能：菜单多角色多用户自定义管理
* 系统配置功能：自定义进行站点配置及第三方参数配置
* 讲师管理功能：讲师列出、添加、修改、删除等功能
* 课程分类管理：分类列出、添加、修改、删除等功能
* 课程管理功能：课程列出、添加、修改、删除等功能
* 订单管理功能：订单的列出，对订单进行分析统计功能
* 用户管理功能：同一时间只允许同一个账号在同一个浏览器登录，防止账号共享
* 轮播管理功能：后台自定义轮播设置，增加营销效果
* 支付功能模块：对接官方支付宝、官方微信
* 其他功能模块：菜单导航功能、友情链接功能等



##### 科友培训系统(kyplatform-education)：[码云地址](https://gitee.com/kyplatformcom/kyplatform-education) | [Github地址](https://github.com/kyplatform/kyplatform-education)

> kyplatform-education是后台工程，核心框架：Spring Cloud Alibaba

##### 前端门户工程(kyplatform-education-web)： [码云地址](https://gitee.com/kyplatformcom/kyplatform-education-web) | [Github地址](https://github.com/kyplatform/kyplatform-education-web)

> kyplatform-education-web是前端门户工程，核心框架：Vuejs + Nuxt.js

##### 后台管理工程(kyplatform-education-admin)：[码云地址](https://gitee.com/kyplatformcom/kyplatform-education-admin) | [Github地址](https://github.com/kyplatform/kyplatform-education-admin)

> kyplatform-education-admin是后台管理工程，核心框架：vue-element-admin

